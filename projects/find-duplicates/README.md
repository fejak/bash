# Find duplicates

This program searches for duplicate files in given directory using the MD5 hash function. If any duplicate files are found, it offers to delete the redundant files and to keep only the oldest versions of the duplicate files. No warranty.

## Dependencies

* **GNU Coreutils**
  * (should be present basically in every Linux distribution)
