#!/bin/bash
#-------------------------------------------------------------------------------
# This program searches for duplicate files in given directory and offers
# removal of redundant files.
#
# No warranty!
#
# Author: gitlab.com/fejak
#-------------------------------------------------------------------------------
# Generates the list of files in the format:
# "md5checksum date filename"
getFileList ()
{
    for i in *; do
        # skip if not regular file
        if [ ! -f "$i" ]; then
            continue
        fi

        local fileDate=$(date -r "$i" +"%Y-%m-%d-%H-%M-%S")
        local fileSum=$(md5sum -- "$i" | awk '{print $1}')
        
        echo "$fileSum"" ""$fileDate"" ""$i"
    done
}
#-------------------------------------------------------------------------------
# Find duplicate files.
# Omit (keep) only the oldest duplicate file.
filterRedundants ()
{
    local fileListSorted="$1"
    
    local prevHash=""
    local matchCnt=0

    while read -r curLine; do
        local curHash=$(echo "$curLine" | cut -d' ' -f1)

        # If current file has the same hash as the previous file,
        # add current filename to the list of redundant files.
        # The first (oldest) file is always omitted.
        if [ "$curHash" == "$prevHash" ]; then
            echo "$curLine" | cut -d' ' -f3-
        fi

        prevHash="$curHash"
    done <<< "$fileListSorted"
}
#-------------------------------------------------------------------------------
deleteFiles ()
{
    local fileList="$1"

    while read -r line; do
        rm -f "$line"
    done <<< "$fileList"
}
#-------------------------------------------------------------------------------
main ()
{
    echo -n "Generating the MD5 hashes..."
    local fileList=$(getFileList)
    echo -ne " Done\nSorting the file list..."
    # Sort files by md5 hash and then by the last modification time
    local fileListSorted=$(echo "$fileList" | sort -k1,1 -k2,2)
    echo -ne " Done\nSearching for duplicate files..."
    local duplicates=$(filterRedundants "$fileListSorted")
    echo " Done"

    if [ -z "$duplicates" ]; then
        echo "No duplicate files found."
        exit 0
    fi
    
    duplicateCnt=$(echo "$duplicates" | wc -l)

    echo "Found ""$duplicateCnt"" redundant files."
    read -p "Do you want to delete them? (y/n) " answer

    if [ "$answer" == "y" -o "$answer" == "Y" ]; then
        deleteFiles "$duplicates"
        echo "Done."
    fi
}
#-------------------------------------------------------------------------------
main
