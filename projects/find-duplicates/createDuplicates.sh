#!/bin/bash
#-------------------------------------------------------------------------------
# This program generates randomly named text files containing random strings.
# Some files will intentionally contain duplicate content (duplicate strings).
# This is for testing of the duplicate file detection script.
#
# Author: gitlab.com/fejak
#-------------------------------------------------------------------------------
# Total number of generated files
FILE_CNT=50
# Max. number of duplicate files
DUPLICATE_MAX=5
#-------------------------------------------------------------------------------
# Check if input is an integer.
# source: https://stackoverflow.com/questions/2210349#2212504
isInteger ()
{
    test "$@" -eq "$@" 2> /dev/null;
}
#-------------------------------------------------------------------------------
# Generate a random alphanumeric string of given length.
alphanum ()
{
    local length="$1"

    cat /dev/urandom \
        | tr -dc 'a-zA-Z0-9' \
        | fold -w "$length" \
        | head -n 1 
}
#-------------------------------------------------------------------------------
# Check whether the input command line parameter is a positive integer.
# If not, assign the default value (FILE_CNT).
checkInput ()
{
    local fileCnt="$1"

    # Check if command line parameter exists
    if [ -z "$fileCnt" ]; then
        fileCnt="$FILE_CNT"
    else
        # Check if command line parameter is an integer
        isInteger "$fileCnt"
        if [ "$?" -ne 0 ]; then
            fileCnt="$FILE_CNT"
        # Check if command line parameter is not a negative integer
        elif [ "$fileCnt" -lt 0 ]; then
            fileCnt="$FILE_CNT"
        fi
    fi

    echo "$fileCnt"
}
#-------------------------------------------------------------------------------
createFiles ()
{
    local fileCnt="$1"

    echo -n "Creating ""$fileCnt"" random files..."

    local duplicateCnt=$(((RANDOM%DUPLICATE_MAX)+1))
    local content=$(alphanum 30)
    
    for i in $(seq 1 "$fileCnt"); do
        local filename=$(alphanum 30)
       
        # paranoid safety measure
        if [ -e "$filename" ]; then
            continue
        fi
        
        echo "$content" > "$filename"

        ((duplicateCnt--))

        if [ "$duplicateCnt" -eq 0 ]; then
            local duplicateCnt=$(((RANDOM%DUPLICATE_MAX)+1))
            local content=$(alphanum 30)
        fi
    done
    
    echo " Done"
}
#-------------------------------------------------------------------------------
main ()
{
    local fileCnt=$(checkInput "$1")

    createFiles "$fileCnt"
}
#-------------------------------------------------------------------------------
main "$1"
