#!/bin/bash
#-------------------------------------------------------------------------------
# This script runs several "time" commands in succession and calculates the
# average "real" time of executing a program.
#
# Usage: ./averageTime.sh [number_of_runs] exe_name
#
# Author: gitlab.com/fejak
#-------------------------------------------------------------------------------
readonly ARG_CNT="$#"
readonly DEC_PNT=$(locale decimal_point) # decimal point separator
RUN_CNT=10
FILENAME=""
#-------------------------------------------------------------------------------
# Check if variable is a number.
isNumber ()
{
    [ "$1" -eq "$1" ] 2>/dev/null
}
#-------------------------------------------------------------------------------
# Check the command line arguments and correctly set the RUN_CNT and FILENAME
# variables.
checkArguments ()
{
    if [ "$ARG_CNT" -eq 0 -o "$ARG_CNT" -gt 2 ]; then
        echo "Usage: ""$0"" [number_of_runs] exe_name"
        exit 1
    fi

    if [ "$ARG_CNT" -eq 1 ]; then
        FILENAME="$1"
    elif [ "$ARG_CNT" -eq 2 ]; then
        isNumber "$1"

        if [ "$?" -ne 0 ]; then
            echo "Error: number_of_runs not a number."
            echo "Usage: ""$0"" [number_of_runs] exe_name"
            exit 1
        fi

        if [ "$1" -lt 1 ]; then
            exit 0
        fi

        RUN_CNT="$1"
        FILENAME="$2"
    fi

    if [ ! -e "$FILENAME" ]; then
        echo "Error: File \"""$FILENAME""\" does not exist."
        exit 1
    fi
}
#-------------------------------------------------------------------------------
main ()
{
    checkArguments "$@"

    # total miliseconds
    local msTotal=0

    for i in $(seq 1 "$RUN_CNT"); do
        local curTime=$({ time ./"$FILENAME" >/dev/null; } 2>&1 \
                        | grep 'real' \
                        | cut -d'm' -f2- \
                        | cut -d's' -f1)

        printf "Run %3d: %s\n" "$i" "$curTime"

        local sec=$(echo "$curTime" | cut -d"$DEC_PNT" -f1)
        local ms=$(echo "$curTime" | cut -d"$DEC_PNT" -f2)

        # convert to decimal (remove the leading zeros)
        sec=$((10#$sec))
        ms=$((10#$ms))

        msTotal=$((msTotal+sec*1000+ms))
    done

    local msAvg=$((msTotal/RUN_CNT))

    local sec=$((msAvg/1000))
    local ms=$((msAvg%1000))

    echo "--------------"
    printf "Average: %d%c%03d\n" "$sec" "$DEC_PNT" "$ms"
}
#-------------------------------------------------------------------------------
main "$@"
