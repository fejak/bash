# Average time

This script runs several "time" commands in succession and calculates the average "real" time of executing a program.

Usage:

* `./averageTime.sh [number_of_runs] exe_name`
