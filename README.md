# Bash projects

Personal tutorials, code snippets and programs in GNU Bash.

## Repository structure

* **learning**
  * Basic features of the language with commentary.
* **practice**
  * Short programs meant to practice some algorithms or language features.
* **projects**
  * More complex programs with some actual useful value.
